const express = require('express')
const router = express.Router()

const authentications = require('../controllers/API/Authentications/authentications')

router.post('/register', authentications.register)

module.exports = router
