const express = require('express')
const router = express.Router()

const seekers = require('../controllers/API/properties/properties')

router.post('/all', seekers.allproperties)

module.exports = router

